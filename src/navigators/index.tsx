import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import {
  EmployeeDetailScreen,
  EmployeesScreen,
  UserProfileScreen,
} from '../screens';
import { ScreenParamsList } from './paramsList';

const Stack = createStackNavigator<ScreenParamsList>();
const Drawer = createDrawerNavigator();

function EmployeeNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: '#6C3ECD',
        headerTitleAlign: 'center',
      }}>
      <Stack.Screen component={EmployeesScreen} name="Employees" />
      <Stack.Screen component={EmployeeDetailScreen} name="EmployeeDetail" />
    </Stack.Navigator>
  );
}

export const MainNavigator = () => (
  <Drawer.Navigator>
    <Drawer.Screen component={EmployeeNavigator} name="Employees" />
    <Drawer.Screen component={UserProfileScreen} name="UserProfile" />
  </Drawer.Navigator>
);

import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import React from 'react';
import { Text, TouchableOpacity, View, Platform } from 'react-native';
import { ScreenParamsList } from '../../navigators/paramsList';
import { IEmployee } from '../../types/employee';
import styles from './styles';

type EmployeesStackProp = StackNavigationProp<ScreenParamsList, 'Employees'>;

interface Props {
  item: IEmployee;
}

type EmployeeItem = {
  employee: IEmployee;
  goToEmployeeDetail: () => void;
};

const isIOSPlatform = Platform.OS === 'ios';

const IOSEmployeeItem = ({ employee, goToEmployeeDetail }: EmployeeItem) => (
  <TouchableOpacity style={styles.employeeItem} onPress={goToEmployeeDetail}>
    <View style={styles.employeeInfo}>
      <Text>
        {employee.firstname} {employee.lastname}
      </Text>
      <Text>{employee.email}</Text>
    </View>
  </TouchableOpacity>
);

const AndroidEmployeeItem = ({
  employee,
  goToEmployeeDetail,
}: EmployeeItem) => (
  <TouchableOpacity style={styles.card} onPress={goToEmployeeDetail}>
    <View style={styles.employeeInfo}>
      <Text>
        {employee.firstname} {employee.lastname}
      </Text>
      <Text>{employee.email}</Text>
    </View>
  </TouchableOpacity>
);

export const Employee = (props: Props) => {
  const { item } = props;
  const { navigate } = useNavigation<EmployeesStackProp>();

  const goToEmployeeDetail = () => {
    navigate('EmployeeDetail', { employee: item });
  };

  if (isIOSPlatform)
    return (
      <IOSEmployeeItem
        employee={item}
        goToEmployeeDetail={goToEmployeeDetail}
      />
    );

  return (
    <AndroidEmployeeItem
      employee={item}
      goToEmployeeDetail={goToEmployeeDetail}
    />
  );
};
